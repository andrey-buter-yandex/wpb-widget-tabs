<?php 

/**
 * Theme Tabbed widget class
 *
 * @since 1.0
 */
class WPB_Widget_Tabbed extends WP_Widget 
{
	public $types = array(
		'views'     => 'Trending',
		'comments'  => 'Comments',
		'playbacks' => 'Played'
	);

	public $widget_base_id = 'wpbw_widget_tabs';

	function __construct() 
	{
		$widget_ops = array( 
			'classname'   => 'js-wpbw-widget-tabs widget_tabs', 
			'description' => __( "Multiple tabs widget" ) 
		);

		parent::__construct( $this->widget_base_id, __( 'WPB Tabs' ), $widget_ops );

		// $this->alt_option_name = 'widget_tabs';

		$this->types = apply_filters( 'wpbw_tabs_widget_register_types', $this->types );

		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	function widget( $args, $instance ) 
	{
		$cache = wp_cache_get( $this->widget_base_id, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		extract( $args );

		// Collect popular parts content and tabs titles
		$type_content = $content = $titles = '';

		$enabled_parts = 0;

		foreach ( $this->types as $type => $title ) {
			$method = 'get_'. $type;

			if ( method_exists( $this, $method ) )
				$type_content = $this->$method( $instance );

			$type_content = apply_filters( 'wpbw_tabs_widget_get_'. $type .'_content', $type_content, $instance );

			if ( !$type_content )
				continue;

			$enabled_parts++;

			$titles .= '<a href="#'. $type . $args['widget_id'] .'" class="js-wpbw-tab-title tab-title" rel="'. $args['widget_id'] .'">'. $title .'</a>';
			$content .= '<div class="tab-content recent-posts-list posts-list" id="'. $type . $args['widget_id'] .'" rel="'. $args['widget_id'] .'">'. $type_content .'</div>';
		}

		if ( !empty( $content ) ) {
			echo $before_widget;
			echo '<span class="tabs-titles">'. $titles .'</span><div class="js-wpbw-tabs-content wpbw-tabs-content widget_recent_posts">'. $content .'</div>';
			echo $after_widget;
		}

		$cache[$args['widget_id']] = ob_get_flush();

		wp_cache_set( $this->widget_base_id, $cache, 'widget' );
	}

	/**
	 * Get popular posts tab
	 * @param array  widget instance
	 * @return mixed array with tab title and content or false if no posts were found
	 */
	function get_views( $instance ) 
	{
		// Check if playbacks need to be included
		if ( !isset( $instance['include'] ) || ( is_array( $instance['include'] ) && !in_array( 'views', $instance['include'] ) ) )
			return;

		$popular = new WP_Query( array(
			'post_type'      => 'post',
			'posts_per_page' => $instance['number'],
			'orderby'        => 'meta_value_num',
			'meta_key'       => 'views'
		));

		if ( !$popular->have_posts() )
			return false;

		ob_start();
		while ( $popular->have_posts() ) : $popular->the_post();
			get_theme_part( 'widget/recent', 'most-viewed' );
		endwhile;
		$content = ob_get_clean();

		wp_reset_postdata();

		return $content;
	}

	/**
	 * Get popular posts tab
	 * @param array  widget instance
	 * @return mixed array with tab title and content or false if no posts were found
	 */
	function get_comments( $instance ) 
	{
		// Check if playbacks need to be included
		if ( !isset( $instance['include'] ) )
			return;

		if ( is_array( $instance['include'] ) AND !in_array( 'comments', $instance['include'] ) )
			return;

		$recent = new WP_Query(array(
			'post_type'      => 'post',
			'posts_per_page' => $instance['number'],
			'orderby'        => 'comment_count',
			'order'          => 'DESC'
		));

		if ( !$recent->have_posts() )
			return false;

		$title = isset( $instance['title_recent'] ) ? $instance['title_recent'] : __( 'Recent', 'itmwp2' );

		ob_start();
		while ( $recent->have_posts() ) : $recent->the_post();
			get_theme_part( 'widget/recent', 'comment' );
		endwhile;
		$content = ob_get_clean();

		wp_reset_postdata();

		return $content;
	}

	function flush_widget_cache() 
	{
		wp_cache_delete( $this->widget_base_id, 'widget' );
	}


	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['number']  = (int) $new_instance['number'];
		$instance['include'] = array();

		foreach ( $this->types as $key => $value ) {
			if ( isset( $new_instance['include-' . $key] ) ) {
				$instance['include'][] = $key;
			}
		}

		$this->flush_widget_cache();

		// ?????????
		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['widget_tabs'] ) )
			delete_option( 'widget_tabs' );

		return $instance;
	}

	function form( $instance ) 
	{
		$number  = isset( $instance['number'] )  ? absint( $instance['number'] ) : 5;
		$include = isset( $instance['include'] ) ? $instance['include'] : array();
?>

		<p>
			<label><?php _e( 'Include:' ) ?></label>
		</p>
		<p>
			<?php foreach ( $this->types as $key => $title ): ?>
			<input type="checkbox" name="<?php echo $this->get_field_name( 'include-' . $key ); ?>" id="<?php echo $this->get_field_id( $key ); ?>" value="<?php echo $key; ?>" <?php checked( in_array( $key, $include ) ) ?>> <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $title; ?></label><br />
			<?php endforeach ?>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>
<?php
	}
}