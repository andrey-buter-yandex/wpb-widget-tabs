<?php
/*
Plugin Name: WPB Tabbed Widget
Description: 
Author: WPB
Version: 1.0
Author URI: http://wordpressforbroadcasters.com/
License: GPLv2 or later
Text Domain: rssi
Domain Path: /i18n
*/

/**
 * Current plugin version
 * Each time on plugin initialization 
 * we are checking this version with one stored in plugin options
 * and if they don't match plugin update hook will be fired
 *
 * @since 2.1
 */
if ( !defined( 'WPBW_T_VERSION' ) )
	define( 'WPBW_T_VERSION', '1.0' );

/**
 * The name of the SocialFlow Core file
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_T_FILE' ) )
	define( 'WPBW_T_FILE', __FILE__ );

/**
 * Absolute location of SocialFlow Plugin
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_T_ABSPATH' ) )
	define( 'WPBW_T_ABSPATH', dirname( WPBW_T_FILE ) );

/**
 * The name of the SocialFlow directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_T_DIRNAME' ) )
	define( 'WPBW_T_DIRNAME', basename( WPBW_T_ABSPATH ) );

/*
 * The name of the SocialFlow directory
 *
 * @since 2.0
 */
if ( !defined( 'WPBW_T_URI' ) )
	define( 'WPBW_T_URI', plugins_url( '', WPBW_T_FILE ) );


/**
 * WPB_Widget_Tabs_Plugin
 */
WPB_Widget_Tabs_Plugin::load();

class WPB_Widget_Tabs_Plugin
{	
	public static function load()
	{
		add_action( 'plugins_loaded',     array( __CLASS__, 'load_templates' ) );
		add_action( 'widgets_init',       array( __CLASS__, 'widget_init'    ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts_styles' ) );
	}

	function load_templates()
	{
		require_once( WPBW_T_ABSPATH . '/inc/widget.php' );
	}

	function widget_init()
	{
		register_widget( 'WPB_Widget_Tabbed' );
	}

	function scripts_styles()
	{
		$path = WPBW_T_URI .'/assets';

		wp_enqueue_script( 'wpbw-tabs-common', $path .'/wpb-tabbed-widget-common.js', array( 'jquery' ), '6.2.1', true );
	}
}