"use strict";

jQuery(document).ready(function($) {

	$('.js-wpbw-widget-tabs').map(function(){
		var tabs    = $(this).find('.js-wpbw-tab-title'),
			content = $(this).find('.js-wpbw-tabs-content');

		// var all = $('').add(tabs).add(content),
		var currentClass = 'selected';

		tabs.on('click', function(e) {
			var tab = $(this),
				hash = tab.attr('href');

			e.preventDefault();

			selectTab(tab, currentClass);
			selectTab(content.find(hash), currentClass);
		});

		// Trigger initial click 
		tabs.filter(':first-child').each(function(i, element) {
			$(element).trigger('click');
		});

		function selectTab(el, currentClass) {
			el.addClass(currentClass).siblings().removeClass(currentClass);
		}
	});

}) // End of jQuery shell function